package org.example;

import org.example.service.SimpleCalculator;
import org.example.view.CalculatorCli;

public class Main {

  public static void main(String[] args) {
    CalculatorCli cli = new CalculatorCli(new SimpleCalculator());
    cli.run();
  }
}