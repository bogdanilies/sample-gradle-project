package org.example.exception;

public class InvalidNumberForSqrtException extends RuntimeException {

  public InvalidNumberForSqrtException(String message) {
    super(message);
  }
}
