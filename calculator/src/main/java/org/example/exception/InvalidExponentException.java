package org.example.exception;

public class InvalidExponentException extends RuntimeException {

  public InvalidExponentException(String message) {
    super(message);
  }
}
