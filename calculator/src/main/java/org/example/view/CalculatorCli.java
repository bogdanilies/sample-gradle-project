package org.example.view;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;
import org.example.exception.DivisionByZeroException;
import org.example.exception.InvalidExponentException;
import org.example.exception.InvalidNumberForSqrtException;
import org.example.service.SimpleCalculator;

public class CalculatorCli {

  public static final String INVALID_CMD_MSG = "Invalid command!\n";

  public static final String INVALID_INPUT_MSG = "Invalid input!\n";

  private final Scanner scanner;

  private final SimpleCalculator calculator;

  public CalculatorCli(SimpleCalculator calculator) {
    scanner = new Scanner(System.in);
    this.calculator = calculator;
  }

  public void run() {
    while (true) {
      showMenu();

      String input = scanner.next();

      switch (input) {
        case "1":
          add();
          break;
        case "2":
          subtract();
          break;
        case "3":
          multiply();
          break;
        case "4":
          divide();
          break;
        case "5":
          max();
          break;
        case "6":
          min();
          break;
        case "7":
          sqrt();
          break;
        case "8":
          getAbsoluteValue();
          break;
        case "9":
          raiseToPower();
          break;
        case "x":
        case "X":
          System.out.println("Exiting...");
          System.exit(0);
        default:
          System.out.println(INVALID_CMD_MSG);
      }
    }
  }

  private void raiseToPower() {
    try {
      System.out.print("Base: ");
      BigDecimal base = scanner.nextBigDecimal();
      System.out.print("Exponent: ");
      Integer exp = scanner.nextInt();

      System.out.println("Result: " + calculator.raiseToPower(base, exp));
    } catch (InputMismatchException ex) {
      System.out.println(INVALID_INPUT_MSG);
    } catch (InvalidExponentException ex) {
      scanner.nextLine();
      System.out.println(ex.getMessage());
    }
  }

  private void getAbsoluteValue() {
    try {
      System.out.print("Number: ");
      BigDecimal no = scanner.nextBigDecimal();

      System.out.println("Result: " + calculator.getAbsoluteValue(no));
    } catch (InputMismatchException ex) {
      scanner.nextLine();
      System.out.println(INVALID_INPUT_MSG);
    }
  }

  private void sqrt() {
    try {
      System.out.print("Number: ");
      BigDecimal base = scanner.nextBigDecimal();

      System.out.println("Square root: " + calculator.sqrt(base));
    } catch (InputMismatchException ex) {
      System.out.println(INVALID_INPUT_MSG);
    } catch (InvalidNumberForSqrtException ex) {
      scanner.nextLine();
      System.out.println(ex.getMessage());
    }
  }

  private void min() {
    try {
      System.out.print("First number: ");
      BigDecimal no1 = scanner.nextBigDecimal();
      System.out.print("Second number: ");
      BigDecimal no2 = scanner.nextBigDecimal();

      System.out.println("Minimum: " + calculator.min(no1, no2));
    } catch (InputMismatchException ex) {
      System.out.println(INVALID_INPUT_MSG);
    } catch (DivisionByZeroException ex) {
      scanner.nextLine();
      System.out.println(ex.getMessage());
    }
  }

  private void max() {
    try {
      System.out.print("First number: ");
      BigDecimal no1 = scanner.nextBigDecimal();
      System.out.print("Second number: ");
      BigDecimal no2 = scanner.nextBigDecimal();

      System.out.println("Maximum: " + calculator.max(no1, no2));
    } catch (InputMismatchException ex) {
      System.out.println(INVALID_INPUT_MSG);
    } catch (DivisionByZeroException ex) {
      scanner.nextLine();
      System.out.println(ex.getMessage());
    }
  }

  private void divide() {
    try {
      System.out.print("First number: ");
      BigDecimal no1 = scanner.nextBigDecimal();
      System.out.print("Second number: ");
      BigDecimal no2 = scanner.nextBigDecimal();

      System.out.println("Result: " + calculator.multiply(no1, no2));
    } catch (InputMismatchException ex) {
      System.out.println(INVALID_INPUT_MSG);
    } catch (DivisionByZeroException ex) {
      scanner.nextLine();
      System.out.println(ex.getMessage());
    }
  }

  private void multiply() {
    try {
      System.out.print("First number: ");
      BigDecimal no1 = scanner.nextBigDecimal();
      System.out.print("Second number: ");
      BigDecimal no2 = scanner.nextBigDecimal();

      System.out.println("Result: " + calculator.multiply(no1, no2));
    } catch (InputMismatchException ex) {
      scanner.nextLine();
      System.out.println(INVALID_INPUT_MSG);
    }
  }

  private void subtract() {
    try {
      System.out.print("First number: ");
      BigDecimal no1 = scanner.nextBigDecimal();
      System.out.print("Second number: ");
      BigDecimal no2 = scanner.nextBigDecimal();

      System.out.println("Difference: " + calculator.subtract(no1, no2));
    } catch (InputMismatchException ex) {
      scanner.nextLine();
      System.out.println(INVALID_INPUT_MSG);
    }
  }

  public void add() {
    try {
      System.out.print("First number: ");
      BigDecimal no1 = scanner.nextBigDecimal();
      System.out.print("Second number: ");
      BigDecimal no2 = scanner.nextBigDecimal();

      System.out.println("Sum: " + calculator.add(no1, no2));
    } catch (InputMismatchException ex) {
      scanner.nextLine();
      System.out.println(INVALID_INPUT_MSG);
    }
  }

  private void showMenu() {
    System.out.println("1. Add");
    System.out.println("2. Subtract");
    System.out.println("3. Multiply");
    System.out.println("4. Divide");
    System.out.println("5. Maximum");
    System.out.println("6. Minimum");
    System.out.println("7. Square root");
    System.out.println("8. Get absolute value");
    System.out.println("9. Raise to power");
    System.out.println("Press X to exit");
    System.out.println();
  }
}
