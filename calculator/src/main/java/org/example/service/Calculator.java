package org.example.service;

import java.math.BigDecimal;

public interface Calculator {

  BigDecimal add(BigDecimal no1, BigDecimal no2);

  BigDecimal subtract(BigDecimal no1, BigDecimal no2);

  BigDecimal multiply(BigDecimal no1, BigDecimal no2);

  BigDecimal divide(BigDecimal no1, BigDecimal no2);

  BigDecimal max(BigDecimal no1, BigDecimal no2);

  BigDecimal min(BigDecimal no1, BigDecimal no2);

  BigDecimal sqrt(BigDecimal no);
}
