package org.example.service;

import static java.lang.Math.abs;

import java.math.BigDecimal;
import org.example.exception.DivisionByZeroException;
import org.example.exception.InvalidExponentException;
import org.example.exception.InvalidNumberForSqrtException;

public class SimpleCalculator implements Calculator {

  public static final String DIV_BY_ZERO_MSG = "Cannot divide by zero";

  public static final String INVALID_EXP_MSG =
      "Exponent must be a positive integer";

  public static final String INVALID_NO_FOR_SQRT_MSG =
      "Cannot find square root of negative number";

  /**
   * Adds two numbers.
   *
   * @param no1 The first number to add.
   * @param no2 The second number to add.
   * @return The sum of the two numbers.
   */
  @Override
  public BigDecimal add(BigDecimal no1, BigDecimal no2) {
    return no1.add(no2);
  }

  /**
   * Subtracts two numbers.
   *
   * @param no1 The number to subtract from.
   * @param no2 The subtracted number.
   * @return The difference of the two numbers.
   */
  @Override
  public BigDecimal subtract(BigDecimal no1, BigDecimal no2) {
    return no1.subtract(no2);
  }

  /**
   * Multiplies two numbers.
   *
   * @param no1 The first item to multiply.
   * @param no2 The second item to multiply.
   * @return The product of the two numbers.
   */
  @Override
  public BigDecimal multiply(BigDecimal no1, BigDecimal no2) {
    return no1.multiply(no2);
  }

  /**
   * Divides a number by another.
   *
   * @param no1 The number to be divided.
   * @param no2 The number to divide by.
   * @return The result of the division of the two numbers.
   */
  @Override
  public BigDecimal divide(BigDecimal no1, BigDecimal no2) {
    if (no2.equals(BigDecimal.ZERO)) {
      throw new DivisionByZeroException(DIV_BY_ZERO_MSG);
    }

    return no1.divide(no2);
  }

  /**
   * Finds the maximum of two numbers.
   *
   * @param no1 The first number to compare.
   * @param no2 The second number to compare.
   * @return The maximum of the two numbers.
   */
  @Override
  public BigDecimal max(BigDecimal no1, BigDecimal no2) {
    if (no1.compareTo(no2) > 0) {
      return no1;
    }

    return no2;
  }

  /**
   * Finds the minimum of two numbers.
   *
   * @param no1 The first number to compare.
   * @param no2 The second number to compare.
   * @return The minimum of the two numbers.
   */
  @Override
  public BigDecimal min(BigDecimal no1, BigDecimal no2) {
    if (no1.compareTo(no2) < 0) {
      return no1;
    }

    return no2;
  }

  /**
   * Finds the square root of a number.
   *
   * @param no The number for which to calculate the square root.
   * @return The square root of the number.
   */
  @Override
  public BigDecimal sqrt(BigDecimal no) {
    if (no.compareTo(BigDecimal.ZERO) < 0) {
      throw new InvalidNumberForSqrtException(INVALID_NO_FOR_SQRT_MSG);
    }

    double sqrt = no.doubleValue();
    double precision = 1e-15;

    while (abs(sqrt - no.doubleValue() / sqrt) > precision * sqrt) {
      sqrt = (no.doubleValue() / sqrt + sqrt) / 2.0;
    }

    return new BigDecimal(sqrt);
  }

  /**
   * Computes the absolute value of a number.
   *
   * @param no The number for which to calculate the absolute value.
   * @return The absolute value of the number.
   */
  public BigDecimal getAbsoluteValue(BigDecimal no) {
    BigDecimal abs = no;

    if (no.compareTo(BigDecimal.ZERO) < 0) {
      abs = no.multiply(BigDecimal.valueOf(-1));
    }

    return abs;
  }

  /**
   * Raises a number to a power.
   *
   * @param base The number to raise to a power.
   * @param exp  The power to raise the number by.
   * @return The result of raising the number to the power.
   */
  public BigDecimal raiseToPower(BigDecimal base, Integer exp) {
    if (exp < 0) {
      throw new InvalidExponentException(INVALID_EXP_MSG);
    }

    BigDecimal result = BigDecimal.ONE;

    for (int i = 0; i < exp; i++) {
      result = result.multiply(base);
    }

    return result;
  }
}
