package service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import org.example.exception.DivisionByZeroException;
import org.example.exception.InvalidExponentException;
import org.example.exception.InvalidNumberForSqrtException;
import org.example.service.SimpleCalculator;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

  private static final BigDecimal NO1 = new BigDecimal("26.381");

  private static final BigDecimal NO2 = new BigDecimal("2.3");

  private static final BigDecimal NO_FOR_SQRT = new BigDecimal("4");

  private static final BigDecimal SQRT_RESULT = new BigDecimal("2");

  private static final BigDecimal NEGATIVE_NO = new BigDecimal("-2.3");

  private static final Integer EXP = 4;

  private static final BigDecimal SUM = new BigDecimal("28.681");

  private static final BigDecimal DIFFERENCE = new BigDecimal("24.081");

  private static final BigDecimal PRODUCT = new BigDecimal("60.6763");

  private static final BigDecimal DIVISION_RESULT = new BigDecimal("11.47");

  private static final BigDecimal RAISE_TO_POWER_RESULT = new BigDecimal(
      "27.9841");

  private final SimpleCalculator calculator = new SimpleCalculator();

  @Test
  public void addValidNumbersReturnsSum() {
    BigDecimal actual = calculator.add(NO1, NO2);

    assertEquals(SUM, actual);
  }

  @Test
  public void subtractValidNumbersReturnsDifference() {
    BigDecimal actual = calculator.subtract(NO1, NO2);

    assertEquals(DIFFERENCE, actual);
  }

  @Test
  public void multiplyValidNumbersReturnsProduct() {
    BigDecimal actual = calculator.multiply(NO1, NO2);

    assertEquals(PRODUCT, actual);
  }

  @Test
  public void divideValidNumbersReturnsResultOfDivision() {
    BigDecimal actual = calculator.divide(NO1, NO2);

    assertEquals(DIVISION_RESULT, actual);
  }

  @Test
  public void divideInvalidNumberThrowsDivisionByZeroException() {
    Exception exception = assertThrows(DivisionByZeroException.class,
        () -> calculator.divide(NO1, BigDecimal.ZERO));

    assertEquals(SimpleCalculator.DIV_BY_ZERO_MSG, exception.getMessage());
  }

  @Test
  public void maxFirstNumberGreaterReturnFirstNumber() {
    BigDecimal actual = calculator.max(NO1, NO2);

    assertEquals(NO1, actual);
  }

  @Test
  public void maxSecondNumberGreaterReturnSecondNumber() {
    BigDecimal actual = calculator.max(NO2, NO1);

    assertEquals(NO1, actual);
  }

  @Test
  public void minFirstNumberGreaterReturnSecondNumber() {
    BigDecimal actual = calculator.min(NO1, NO2);

    assertEquals(NO2, actual);
  }

  @Test
  public void minSecondNumberGreaterReturnFirstNumber() {
    BigDecimal actual = calculator.min(NO2, NO1);

    assertEquals(NO2, actual);
  }

  @Test
  public void sqrtValidNumberReturnsSqrt() {
    BigDecimal actual = calculator.sqrt(NO_FOR_SQRT);

    assertEquals(SQRT_RESULT, actual);
  }

  @Test
  public void sqrtNegativeNumberThrowsInvalidNumberForSqrtException() {
    Exception exception = assertThrows(InvalidNumberForSqrtException.class,
        () -> calculator.sqrt(NEGATIVE_NO));

    assertEquals(SimpleCalculator.INVALID_NO_FOR_SQRT_MSG, exception.getMessage());
  }

  @Test
  public void getAbsoluteValuePositiveNumberStaysTheSame() {
    BigDecimal actual = calculator.getAbsoluteValue(NO1);

    assertEquals(NO1, actual);
  }

  @Test
  public void getAbsoluteValueNegativeNumberReturnsOpposite() {
    BigDecimal actual = calculator.getAbsoluteValue(NEGATIVE_NO);

    assertEquals(NEGATIVE_NO.abs(), actual);
  }

  @Test
  public void raiseToPowerValidNumbersReturnsResultOfRaiseToPower() {
    BigDecimal actual = calculator.raiseToPower(NO2, EXP);

    assertEquals(RAISE_TO_POWER_RESULT, actual);
  }

  @Test
  public void raiseToPowerInvalidExponentThrowsInvalidExponentException() {
    Exception exception = assertThrows(InvalidExponentException.class,
        () -> calculator.raiseToPower(NO2, -EXP));

    assertEquals(SimpleCalculator.INVALID_EXP_MSG, exception.getMessage());
  }
}
