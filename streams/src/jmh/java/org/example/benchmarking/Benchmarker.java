package org.example.benchmarking;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.example.service.DoubleService;
import org.example.service.PrimitiveService;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class Benchmarker {

  private static final int ELEMENT_COUNT = 100000000;

  @State(Scope.Benchmark)
  public static class ObjectState {

    DoubleService service() {
      return new DoubleService();
    }

    List<Double> list() {
      List<Double> list = new ArrayList<>();

      for (int i = 0; i < ELEMENT_COUNT; i++) {
        list.add((double) i);
      }

      return list;
    }

  }

  @State(Scope.Benchmark)
  public static class PrimitiveState {

    PrimitiveService service() {
      return new PrimitiveService();
    }

    double[] list() {
      double[] list = new double[ELEMENT_COUNT];

      for (int i = 0; i < ELEMENT_COUNT; i++) {
        list[i] = i;
      }

      return list;
    }

  }

  @Benchmark
  public void objectComputeSumDoubleObject(Blackhole consumer,
      ObjectState state) {
    consumer.consume(state.service().computeSumDoubleObject(state.list()));
  }

  @Benchmark
  public void objectComputeAvgDoubleObject(Blackhole consumer,
      ObjectState state) {
    consumer.consume(state.service().computeAverageDoubleObject(state.list()));
  }

  @Benchmark
  public void objectPrintTop10PercentBiggestDoubleObject(ObjectState state) {
    PrintStream originalOut = System.out;
    ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outContent));

    state.service().printTop10PercentBiggestDoubleObject(state.list());

    System.setOut(originalOut);
  }

  @Benchmark
  public void objectComputeSumDoublePrimitive(Blackhole consumer,
      PrimitiveState state) {
    consumer.consume(state.service().computeSumDoublePrimitive(state.list()));
  }

  @Benchmark
  public void objectComputeAvgDoublePrimitive(Blackhole consumer,
      PrimitiveState state) {
    consumer.consume(state.service().computeAverageDoublePrimitive(state.list()));
  }

  @Benchmark
  public void objectPrintTop10PercentBiggestDoublePrimitive(
      PrimitiveState state) {
    PrintStream originalOut = System.out;
    ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outContent));

    state.service().printTop10PercentBiggestDoublePrimitive(state.list());

    System.setOut(originalOut);
  }

}
