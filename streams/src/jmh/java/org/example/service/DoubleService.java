package org.example.service;

import java.util.Comparator;
import java.util.List;

public class DoubleService {

  public Double computeSumDoubleObject(List<Double> numbers) {
    return numbers.stream().reduce(0D, Double::sum);
  }

  public Double computeAverageDoubleObject(List<Double> numbers) {
    return numbers.stream().reduce(0D, Double::sum) / (numbers.size());
  }

  public void printTop10PercentBiggestDoubleObject(List<Double> numbers) {
    numbers.stream().sorted(Comparator.reverseOrder())
        .limit((long) (numbers.size() * 0.1)).forEach(System.out::println);
  }

}
