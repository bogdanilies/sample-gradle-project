package org.example.service;

import java.util.Arrays;
import java.util.Comparator;
import net.mintern.primitive.Primitive;

public class PrimitiveService {

  public double computeSumDoublePrimitive(double[] numbers) {
    return Arrays.stream(numbers).reduce(0D, Double::sum);
  }

  public double computeAverageDoublePrimitive(double[] numbers) {
    return Arrays.stream(numbers).average().getAsDouble();
  }

  public void printTop10PercentBiggestDoublePrimitive(double[] numbers) {
    Primitive.sort(numbers, (d1, d2) -> Double.compare(d2, d1), false);
    long count = Arrays.stream(numbers).count();
    Arrays.stream(numbers).limit((long) (count * 0.1))
        .forEach(System.out::println);
  }

}
