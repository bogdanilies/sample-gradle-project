package org.example.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import net.mintern.primitive.Primitive;

public class Service {

  public BigDecimal computeSum(List<BigDecimal> bigDecimals) {
    return bigDecimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  public BigDecimal computeAverage(List<BigDecimal> bigDecimals) {
    return bigDecimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add)
        .divide(BigDecimal.valueOf(bigDecimals.size()));
  }

  public void printTop10PercentBiggest(List<BigDecimal> bigDecimals) {
    bigDecimals.stream().sorted(Comparator.reverseOrder())
        .limit((long) (bigDecimals.size() * 0.1)).forEach(System.out::println);
  }

  public Double computeSumDoubleObject(List<Double> numbers) {
    return numbers.stream().reduce(0D, Double::sum);
  }

  public Double computeAverageDoubleObject(List<Double> numbers) {
    return numbers.stream().reduce(0D, Double::sum) / (numbers.size());
  }

  public void printTop10PercentBiggestDoubleObject(List<Double> numbers) {
    numbers.stream().sorted(Comparator.reverseOrder())
        .limit((long) (numbers.size() * 0.1)).forEach(System.out::println);
  }

  public double computeSumDoublePrimitive(double[] numbers) {
    double sum = 0D;

    for (double number : numbers) {
      sum += number;
    }

    return sum;
  }

  public double computeAverageDoublePrimitive(double[] numbers) {
    double sum = 0D;
    int count = 0;

    for (double number : numbers) {
      sum += number;
      count++;
    }

    return sum / count;
  }

  public void printTop10PercentBiggestDoublePrimitive(double[] numbers,
      int count) {
    Primitive.sort(numbers, (d1, d2) -> Double.compare(d2, d1), false);

    for (int i = 0; i < (long) (count * 0.1); i++) {
      System.out.println(numbers[i]);
    }
  }

}
