package org.example.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Test;

public class BigDecimalServiceTest {

  private static final List<BigDecimal> BIG_DECIMALS = List.of(
      new BigDecimal("1.1"),
      new BigDecimal("2.2"),
      new BigDecimal("3.3"),
      new BigDecimal("4.4"),
      new BigDecimal("5.5"),
      new BigDecimal("6.6"),
      new BigDecimal("7.7"),
      new BigDecimal("8.8"),
      new BigDecimal("9.9"),
      new BigDecimal("10.10"));

  private static final BigDecimal EXPECTED_SUM = new BigDecimal("59.60");

  private static final BigDecimal EXPECTED_AVG = new BigDecimal("5.96");

  private static final String EXPECTED_PRINT_RESULT = "10.10";

  private final PrintStream STD_OUT = System.out;
  private final ByteArrayOutputStream OUTPUT_STREAM_CAPTOR =
      new ByteArrayOutputStream();

  private final BigDecimalService service = new BigDecimalService();

  @Test
  public void computeSumValidListReturnsSum() {
    BigDecimal actual = service.computeSum(BIG_DECIMALS);

    assertEquals(EXPECTED_SUM, actual);
  }

  @Test
  public void computeAvgValidListReturnsSum() {
    BigDecimal actual = service.computeAverage(BIG_DECIMALS);

    assertEquals(EXPECTED_AVG, actual);
  }

  @Test
  public void printTop10PercentBiggestValidListPrintsBiggest() {
    System.setOut(new PrintStream(OUTPUT_STREAM_CAPTOR));

    service.printTop10PercentBiggest(BIG_DECIMALS);

    assertEquals(EXPECTED_PRINT_RESULT, OUTPUT_STREAM_CAPTOR.toString().trim());

    System.setOut(STD_OUT);
  }

}
