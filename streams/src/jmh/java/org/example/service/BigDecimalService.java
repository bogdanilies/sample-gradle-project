package org.example.service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

public class BigDecimalService {

  public BigDecimal computeSum(List<BigDecimal> bigDecimals) {
    return bigDecimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  public BigDecimal computeAverage(List<BigDecimal> bigDecimals) {
    return bigDecimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add)
        .divide(BigDecimal.valueOf(bigDecimals.size()));
  }

  public void printTop10PercentBiggest(List<BigDecimal> bigDecimals) {
    bigDecimals.stream().sorted(Comparator.reverseOrder())
        .limit((long) (bigDecimals.size() * 0.1)).forEach(System.out::println);
  }

}
