package org.example.repository;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import org.example.model.Identifiable;

public class ObjectOpenHashSetBasedRepository<T extends Identifiable<I>, I>
    implements InMemoryRepository<T, I> {

  private final ObjectOpenHashSet<T> set;

  public ObjectOpenHashSetBasedRepository() {
    this.set = new ObjectOpenHashSet<>();
  }

  @Override
  public void add(T t) {
    set.add(t);
  }

  @Override
  public boolean contains(T t) {
    return set.contains(t);
  }

  @Override
  public void remove(T t) {
    set.remove(t);
  }
}
