package org.example.repository;

import org.eclipse.collections.impl.list.mutable.FastList;
import org.example.model.Identifiable;

public class FastListBasedRepository<T extends Identifiable<I>, I> implements
    InMemoryRepository<T, I> {

  private final FastList<T> list;

  public FastListBasedRepository() {
    this.list = new FastList<>();
  }

  @Override
  public void add(T t) {
    list.add(t);
  }

  @Override
  public boolean contains(T t) {
    return list.contains(t);
  }

  @Override
  public void remove(T t) {
    list.remove(t);
  }
}
