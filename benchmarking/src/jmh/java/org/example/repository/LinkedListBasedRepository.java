package org.example.repository;

import java.util.LinkedList;
import java.util.List;
import org.example.model.Identifiable;

public class LinkedListBasedRepository<T extends Identifiable<I>, I> implements
    InMemoryRepository<T, I> {

  private final List<T> list;

  public LinkedListBasedRepository() {
    this.list = new LinkedList<>();
  }

  @Override
  public void add(T t) {
    list.add(t);
  }

  @Override
  public boolean contains(T t) {
    return list.contains(t);
  }

  @Override
  public void remove(T t) {
    list.remove(t);
  }
}
