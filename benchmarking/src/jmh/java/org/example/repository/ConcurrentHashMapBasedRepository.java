package org.example.repository;

import java.util.concurrent.ConcurrentHashMap;
import org.example.model.Identifiable;

public class ConcurrentHashMapBasedRepository<T extends Identifiable<I>, I>
    implements InMemoryRepository<T, I> {

  private final ConcurrentHashMap<I, T> map;

  public ConcurrentHashMapBasedRepository() {
    this.map = new ConcurrentHashMap<>();
  }

  @Override
  public void add(T t) {
    map.put(t.getId(), t);
  }

  @Override
  public boolean contains(T t) {
    return map.contains(t);
  }

  @Override
  public void remove(T t) {
    map.remove(t.getId());
  }
}
