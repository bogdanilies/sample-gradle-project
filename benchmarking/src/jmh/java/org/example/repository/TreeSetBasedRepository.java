package org.example.repository;

import java.util.Set;
import java.util.TreeSet;
import org.example.model.Identifiable;

public class TreeSetBasedRepository<T extends Identifiable<I>, I> implements
    InMemoryRepository<T, I> {

  private final Set<T> set;

  public TreeSetBasedRepository() {
    this.set = new TreeSet<>();
  }

  @Override
  public void add(T t) {
    set.add(t);
  }

  @Override
  public boolean contains(T t) {
    return set.contains(t);
  }

  @Override
  public void remove(T t) {
    set.remove(t);
  }
}
