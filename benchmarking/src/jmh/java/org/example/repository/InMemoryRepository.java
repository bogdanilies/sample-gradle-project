package org.example.repository;

import org.example.model.Identifiable;

public interface InMemoryRepository<T extends Identifiable<I>, I> {

  void add(T t);

  boolean contains(T t);

  void remove(T t);
}
