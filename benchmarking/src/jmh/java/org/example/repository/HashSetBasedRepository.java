package org.example.repository;

import java.util.HashSet;
import java.util.Set;
import org.example.model.Identifiable;

public class HashSetBasedRepository<T extends Identifiable<I>, I> implements
    InMemoryRepository<T, I> {

  private final Set<T> set;

  public HashSetBasedRepository() {
    this.set = new HashSet<>();
  }

  @Override
  public void add(T t) {
    set.add(t);
  }

  @Override
  public boolean contains(T t) {
    return set.contains(t);
  }

  @Override
  public void remove(T t) {
    set.remove(t);
  }
}
