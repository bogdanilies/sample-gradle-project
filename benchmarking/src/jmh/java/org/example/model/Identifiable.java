package org.example.model;

public interface Identifiable<T> {

  T getId();
}
