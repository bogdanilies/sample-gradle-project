package org.example.model;

import java.util.Objects;

public class Order implements Comparable<Order>, Identifiable<Integer> {

  private final Integer id;

  private final Integer price;

  private final Integer quantity;

  public Order(Integer id, Integer price, Integer quantity) {
    this.id = id;
    this.price = price;
    this.quantity = quantity;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Order order = (Order) o;
    return Objects.equals(id, order.id) && Objects.equals(price, order.price)
        && Objects.equals(quantity, order.quantity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, price, quantity);
  }

  @Override
  public String toString() {
    return "Order{" +
        "id=" + id +
        ", price=" + price +
        ", quantity=" + quantity +
        '}';
  }

  @Override
  public int compareTo(Order o) {
    return id - o.id;
  }

  @Override
  public Integer getId() {
    return id;
  }
}
