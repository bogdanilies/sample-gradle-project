package org.example.benchmarking;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.example.model.Order;
import org.example.repository.ArrayListBasedRepository;
import org.example.repository.ConcurrentHashMapBasedRepository;
import org.example.repository.FastListBasedRepository;
import org.example.repository.HashSetBasedRepository;
import org.example.repository.LinkedListBasedRepository;
import org.example.repository.ObjectOpenHashSetBasedRepository;
import org.example.repository.TreeSetBasedRepository;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class Benchmarker {

  private static List<Order> orders = new ArrayList<>();

  @State(Scope.Benchmark)
  public static class ArrayListState {

    ArrayListBasedRepository<Order, Integer> arrayListBasedRepository() {
      ArrayListBasedRepository<Order, Integer> arrayListBasedRepo
          = new ArrayListBasedRepository<>();

      for (int i = 0; i < 1000; i++) {
        orders.add(new Order(i, i, i));
      }

      orders.forEach(arrayListBasedRepo::add);

      return arrayListBasedRepo;
    }
  }

  @State(Scope.Benchmark)
  public static class HashSetState {

    HashSetBasedRepository<Order, Integer> hashSetBasedRepository() {
      HashSetBasedRepository<Order, Integer> hashSetBasedRepo
          = new HashSetBasedRepository<>();

      for (int i = 0; i < 1000; i++) {
        orders.add(new Order(i, i, i));
      }

      orders.forEach(hashSetBasedRepo::add);

      return hashSetBasedRepo;
    }
  }

  @State(Scope.Benchmark)
  public static class TreeSetState {

    TreeSetBasedRepository<Order, Integer> treeSetBasedRepository() {
      TreeSetBasedRepository<Order, Integer> treeSetBasedRepo
          = new TreeSetBasedRepository<>();

      for (int i = 0; i < 1000; i++) {
        orders.add(new Order(i, i, i));
      }

      orders.forEach(treeSetBasedRepo::add);

      return treeSetBasedRepo;
    }
  }

  @State(Scope.Benchmark)
  public static class ConcurrentHashMapState {

    ConcurrentHashMapBasedRepository<Order, Integer>
    concurrentHashMapBasedRepository() {
      ConcurrentHashMapBasedRepository<Order, Integer>
          concurrentHashMapBasedRepo = new ConcurrentHashMapBasedRepository<>();

      for (int i = 0; i < 1000; i++) {
        orders.add(new Order(i, i, i));
      }

      orders.forEach(concurrentHashMapBasedRepo::add);

      return concurrentHashMapBasedRepo;
    }
  }

  @State(Scope.Benchmark)
  public static class ObjectOpenHashSetState {

    ObjectOpenHashSetBasedRepository<Order, Integer>
    objectOpenHashSetBasedRepository() {
      ObjectOpenHashSetBasedRepository<Order, Integer>
          objectOpenHashSetBasedRepo = new ObjectOpenHashSetBasedRepository<>();

      for (int i = 0; i < 1000; i++) {
        orders.add(new Order(i, i, i));
      }

      orders.forEach(objectOpenHashSetBasedRepo::add);

      return objectOpenHashSetBasedRepo;
    }
  }

  @State(Scope.Benchmark)
  public static class FastListState {

    FastListBasedRepository<Order, Integer> fastListBasedRepository() {
      FastListBasedRepository<Order, Integer> fastListBasedRepo
          = new FastListBasedRepository<>();

      for (int i = 0; i < 1000; i++) {
        orders.add(new Order(i, i, i));
      }

      orders.forEach(fastListBasedRepo::add);

      return fastListBasedRepo;
    }
  }

  @State(Scope.Benchmark)
  public static class LinkedListState {

    LinkedListBasedRepository<Order, Integer> linkedListBasedRepository() {
      LinkedListBasedRepository<Order, Integer> linkedListBasedRepo
          = new LinkedListBasedRepository<>();

      for (int i = 0; i < 1000; i++) {
        orders.add(new Order(i, i, i));
      }

      orders.forEach(linkedListBasedRepo::add);

      return linkedListBasedRepo;
    }
  }

  @Benchmark
  public void arrayListBasedRepositoryAdd(ArrayListState state) {
    state.arrayListBasedRepository()
        .add(new Order(1, 1, 1));
  }

  @Benchmark
  public boolean arrayListBasedRepositoryContains(ArrayListState state) {
    return state.arrayListBasedRepository()
        .contains(new Order(1, 1, 1));
  }

  @Benchmark
  public void arrayListBasedRepositoryRemove(ArrayListState state) {
    state.arrayListBasedRepository()
        .remove(new Order(1, 1, 1));
  }

  @Benchmark
  public void hashSetBasedRepositoryAdd(HashSetState state) {
    state.hashSetBasedRepository()
        .add(new Order(1, 1, 1));
  }

  @Benchmark
  public boolean hashSetBasedRepositoryContains(HashSetState state) {
    return state.hashSetBasedRepository()
        .contains(new Order(1, 1, 1));
  }

  @Benchmark
  public void hashSetBasedRepositoryRemove(HashSetState state) {
    state.hashSetBasedRepository()
        .remove(new Order(1, 1, 1));
  }

  @Benchmark
  public void treeSetBasedRepositoryAdd(TreeSetState state) {
    state.treeSetBasedRepository()
        .add(new Order(1, 1, 1));
  }

  @Benchmark
  public boolean treeSetBasedRepositoryContains(TreeSetState state) {
    return state.treeSetBasedRepository()
        .contains(new Order(1, 1, 1));
  }

  @Benchmark
  public void treeSetBasedRepositoryRemove(TreeSetState state) {
    state.treeSetBasedRepository()
        .remove(new Order(1, 1, 1));
  }

  @Benchmark
  public void concurrentHashMapBasedRepositoryAdd(
      ConcurrentHashMapState state) {
    state.concurrentHashMapBasedRepository()
        .add(new Order(1, 1, 1));
  }

  @Benchmark
  public boolean concurrentHashMapBasedRepositoryContains(
      ConcurrentHashMapState state) {
    return state.concurrentHashMapBasedRepository()
        .contains(new Order(1, 1, 1));
  }

  @Benchmark
  public void concurrentHashMapBasedRepositoryRemove(
      ConcurrentHashMapState state) {
    state.concurrentHashMapBasedRepository()
        .remove(new Order(1, 1, 1));
  }

  @Benchmark
  public void objectOpenHashSetBasedRepositoryAdd(
      ObjectOpenHashSetState state) {
    state.objectOpenHashSetBasedRepository()
        .add(new Order(1, 1, 1));
  }

  @Benchmark
  public boolean objectOpenHashSetBasedRepositoryContains(
      ObjectOpenHashSetState state) {
    return state.objectOpenHashSetBasedRepository()
        .contains(new Order(1, 1, 1));
  }

  @Benchmark
  public void objectOpenHashSetBasedRepositoryRemove(
      ObjectOpenHashSetState state) {
    state.objectOpenHashSetBasedRepository()
        .remove(new Order(1, 1, 1));
  }

  @Benchmark
  public void fastListBasedRepositoryAdd(FastListState state) {
    state.fastListBasedRepository()
        .add(new Order(1, 1, 1));
  }

  @Benchmark
  public boolean fastListBasedRepositoryContains(FastListState state) {
    return state.fastListBasedRepository()
        .contains(new Order(1, 1, 1));
  }

  @Benchmark
  public void fastListBasedRepositoryRemove(FastListState state) {
    state.fastListBasedRepository()
        .remove(new Order(1, 1, 1));
  }

  @Benchmark
  public void linkedListBasedRepositoryAdd(LinkedListState state) {
    state.linkedListBasedRepository()
        .add(new Order(1, 1, 1));
  }

  @Benchmark
  public boolean linkedListBasedRepositoryContains(LinkedListState state) {
    return state.linkedListBasedRepository()
        .contains(new Order(1, 1, 1));
  }

  @Benchmark
  public void linkedListBasedRepositoryRemove(LinkedListState state) {
    state.linkedListBasedRepository()
        .remove(new Order(1, 1, 1));
  }
}
