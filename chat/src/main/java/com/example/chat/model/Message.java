package com.example.chat.model;

import java.io.Serializable;

public class Message implements Serializable {

    private MessageType type;

    private String senderUser;

    private String targetUser;

    private String content;

    public Message(MessageType type, String senderUser, String targetUser,
                   String content) {
        this.type = type;
        this.senderUser = senderUser;
        this.targetUser = targetUser;
        this.content = content;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getSenderUser() {
        return senderUser;
    }

    public void setSenderUser(String senderUser) {
        this.senderUser = senderUser;
    }

    public String getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(String targetUser) {
        this.targetUser = targetUser;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
