package com.example.chat.model;

public enum MessageType {
    CONN,
    ACC,
    TEXT,
    DISC,
    EXIT

}
