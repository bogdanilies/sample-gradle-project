package com.example.chat.model;

import java.util.List;

public class User {

    private String username;

    private List<String> activeConnections;

    private List<String> pendingConnections;

    public User(String username, List<String> activeConnections,
                List<String> pendingConnections) {
        this.username = username;
        this.activeConnections = activeConnections;
        this.pendingConnections = pendingConnections;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getActiveConnections() {
        return activeConnections;
    }

    public void setActiveConnections(List<String> activeConnections) {
        this.activeConnections = activeConnections;
    }

    public List<String> getPendingConnections() {
        return pendingConnections;
    }

    public void setPendingConnections(List<String> pendingConnections) {
        this.pendingConnections = pendingConnections;
    }

}
