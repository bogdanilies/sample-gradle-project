package com.example.chat.runner;

import com.example.chat.exception.ChatException;
import com.example.chat.exception.MessageException;
import com.example.chat.model.Message;
import com.example.chat.model.MessageType;
import com.example.chat.model.User;
import com.example.chat.service.MessageService;
import com.example.chat.service.ReceivingService;
import com.example.chat.service.SendingService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

@Component
public class ChatRunner implements CommandLineRunner {

    private static final String USERNAME_PAT = "^[a-zA-Z0-9]+$";

    private static final List<String> DISALLOWED_USERNAMES =
            List.of("connect", "disconnect", "accept", "exit");

    private final MessageService messageService;

    private final SendingService sendingService;

    private final ReceivingService receivingService;

    public ChatRunner(MessageService messageService,
                      SendingService sendingService,
                      ReceivingService receivingService) {
        this.messageService = messageService;
        this.sendingService = sendingService;
        this.receivingService = receivingService;
    }

    @Override
    public void run(String... args) {
        Scanner scanner = new Scanner(System.in);
        printWelcomeMessage(determineUser(scanner));

        while (true) {
            try {
                String content = scanner.nextLine();
                Message message = messageService.determineMessageType(content);
                System.out.println(sendingService.sendMessage(message));
                if (MessageType.EXIT.equals(message.getType())) {
                    System.out.println("Exiting...");
                    System.exit(0);
                }
            } catch (MessageException | ChatException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private String determineUser(Scanner scanner) {
        String username = null;

        while (username == null) {
            System.out.print("Choose a username: ");
            username = scanner.nextLine();
            if (!Pattern.matches(USERNAME_PAT, username)
                    || DISALLOWED_USERNAMES.contains(username)) {
                username = null;
            }
        }

        User user = new User(username, new ArrayList<>(), new ArrayList<>());
        messageService.setUser(user);
        sendingService.setUser(user);
        receivingService.setUser(user);

        return username;
    }

    private void printWelcomeMessage(String username) {
        System.out.println("Welcome to the chat, " + username + "!");
        System.out.println("Use '/connect [username]'" +
                "to establish a connection");
        System.out.println("Use '/[username]' to message a connection");
        System.out.println("Use '/disconnect [username]'" +
                "to close a connection");
        System.out.println("Use '/exit' to close all connections and exit");
    }

}
