package com.example.chat.service;

import com.example.chat.exception.ChatException;
import com.example.chat.model.Message;
import com.example.chat.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Service
public class ReceivingService {

    private User user;

    private final ObjectMapper objectMapper;

    public ReceivingService(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @JmsListener(destination = "${jms.topic}")
    public void receiveMessage(String received) throws ChatException {
        try {
            Message message = objectMapper.readValue(received, Message.class);
            if (message.getTargetUser().equals(user.getUsername())) {
                System.out.println(processReceivedMessage(message));
            }
        } catch (JsonProcessingException e) {
            throw new ChatException("Message could not be read");
        }
    }

    private String processReceivedMessage(Message message) {
        switch (message.getType()) {
            case CONN -> {
                return receiveConnMessage(message);
            }
            case ACC -> {
                return receiveAccMessage(message);
            }
            case TEXT -> {
                return receiveTextMessage(message);
            }
            case DISC -> {
                return receiveDiscMessage(message);
            }
            default -> {
                return "";
            }
        }
    }

    private String receiveDiscMessage(Message message) {
        String sender = message.getSenderUser();
        user.getPendingConnections().remove(sender);
        user.getActiveConnections().remove(sender);
        return sender + " closed the connection";
    }

    private String receiveTextMessage(Message message) {
        return "[" + message.getSenderUser() + "]: " + message.getContent();
    }

    private String receiveAccMessage(Message message) {
        String sender = message.getSenderUser();
        user.getPendingConnections().remove(sender);
        user.getActiveConnections().add(sender);
        return sender + " accepted your connection";
    }

    private String receiveConnMessage(Message message) {
        String sender = message.getSenderUser();
        user.getPendingConnections().add(sender);
        return sender + " requested connection. Type '/accept "
                + sender + "' to establish a connection";
    }

}
