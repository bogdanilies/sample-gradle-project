package com.example.chat.service;

import com.example.chat.exception.ChatException;
import com.example.chat.model.Message;
import com.example.chat.model.MessageType;
import com.example.chat.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class SendingService {

    private static final String JSON_ERR_MSG = "Message could not be sent";

    @Value("${jms.topic}")
    private String chatQueue;

    private User user;

    private final JmsTemplate jmsTemplate;

    private final ObjectMapper objectMapper;

    public SendingService(JmsTemplate jmsTemplate, ObjectMapper objectMapper) {
        this.jmsTemplate = jmsTemplate;
        this.objectMapper = objectMapper;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String sendMessage(Message message) throws ChatException {
        switch (message.getType()) {
            case CONN -> {
                return sendConnMessage(message);
            }
            case ACC -> {
                return sendAccMessage(message);
            }
            case TEXT -> {
                return sendTextMessage(message);
            }
            case DISC -> {
                return sendDiscMessage(message);
            }
            case EXIT -> {
                return sendExitMessage(message);
            }
            default -> {
                return "";
            }
        }
    }

    private String sendExitMessage(Message message) throws ChatException {
        for (String targetUser : user.getPendingConnections()) {
            try {
                Message disconnectMessage = new Message(MessageType.DISC,
                        message.getSenderUser(), targetUser, null);
                jmsTemplate.convertAndSend(chatQueue,
                        objectMapper.writeValueAsString(disconnectMessage));
                user.getPendingConnections().remove(targetUser);
            } catch (JsonProcessingException e) {
                throw new ChatException(JSON_ERR_MSG);
            }
        }

        for (String targetUser : user.getActiveConnections()) {
            try {
                Message disconnectMessage = new Message(MessageType.DISC,
                        message.getSenderUser(), targetUser, null);
                jmsTemplate.convertAndSend(chatQueue,
                        objectMapper.writeValueAsString(disconnectMessage));
                user.getPendingConnections().remove(targetUser);
            } catch (JsonProcessingException e) {
                throw new ChatException(JSON_ERR_MSG);
            }
        }

        return "Closed all connections";
    }

    private String sendDiscMessage(Message message) throws ChatException {
        String targetUser = message.getTargetUser();
        if (!user.getPendingConnections().contains(targetUser)
                && !user.getActiveConnections().contains(targetUser)) {
            throw new ChatException("No connection with the selected user");
        }

        try {
            jmsTemplate.convertAndSend(chatQueue,
                    objectMapper.writeValueAsString(message));
            user.getPendingConnections().remove(targetUser);
            user.getActiveConnections().remove(targetUser);
        } catch (JsonProcessingException e) {
            throw new ChatException(JSON_ERR_MSG);
        }

        return "Closed connection with: " + targetUser;
    }

    private String sendTextMessage(Message message) throws ChatException {
        String targetUser = message.getTargetUser();
        if (!user.getActiveConnections().contains(targetUser)) {
            throw new ChatException("No connection with the selected user");
        }

        try {
            jmsTemplate.convertAndSend(chatQueue,
                    objectMapper.writeValueAsString(message));
        } catch (JsonProcessingException e) {
            throw new ChatException(JSON_ERR_MSG);
        }

        return "Message sent to " + targetUser;
    }

    private String sendAccMessage(Message message) throws ChatException {
        String targetUser = message.getTargetUser();
        if (!user.getPendingConnections().contains(targetUser)) {
            throw new ChatException(
                    "The selected user has not requested a connection");
        }

        try {
            jmsTemplate.convertAndSend(chatQueue,
                    objectMapper.writeValueAsString(message));
            user.getPendingConnections().remove(targetUser);
            user.getActiveConnections().add(targetUser);
        } catch (JsonProcessingException e) {
            throw new ChatException(JSON_ERR_MSG);
        }

        return "Established connection with " + targetUser;
    }

    private String sendConnMessage(Message message) throws ChatException {
        String targetUser = message.getTargetUser();
        if (user.getUsername().equals(targetUser)) {
            throw new ChatException("Invalid user");
        }
        if (user.getActiveConnections().contains(targetUser)) {
            throw new ChatException("Connection already established with "
                    + message.getTargetUser());
        }
        if (user.getPendingConnections().contains(targetUser)) {
            throw new ChatException("Connection already requested with "
                    + message.getTargetUser());
        }

        try {
            jmsTemplate.convertAndSend(chatQueue,
                    objectMapper.writeValueAsString(message));
            user.getPendingConnections().add(targetUser);
        } catch (JsonProcessingException e) {
            throw new ChatException(JSON_ERR_MSG);
        }

        return "Requested connection with " + targetUser;
    }

}
