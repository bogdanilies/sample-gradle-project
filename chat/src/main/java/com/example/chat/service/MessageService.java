package com.example.chat.service;

import com.example.chat.exception.MessageException;
import com.example.chat.model.Message;
import com.example.chat.model.MessageType;
import com.example.chat.model.User;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class MessageService {

    private static final String CONN_MSG_PAT = "^/connect [a-zA-Z0-9]+$";

    private static final String ACC_MSG_PAT = "^/accept [a-zA-Z0-9]+$";

    private static final String TXT_MSG_PAT = "^/[a-zA-Z0-9]+ .+$";

    private static final String DISC_MSG_PAT = "^/disconnect [a-zA-Z0-9]+$";

    private static final String EXIT_MSG_PAT = "^/exit$";

    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    public Message determineMessageType(String content)
            throws MessageException {
        if (Pattern.matches(CONN_MSG_PAT, content)) {
            return new Message(MessageType.CONN, user.getUsername(),
                    content.substring(9), null);
        }
        if (Pattern.matches(DISC_MSG_PAT, content)) {
            return new Message(MessageType.DISC, user.getUsername(),
                    content.substring(12), null);
        }
        if (Pattern.matches(ACC_MSG_PAT, content)) {
            return new Message(MessageType.ACC, user.getUsername(),
                    content.substring(8), null);
        }
        if (Pattern.matches(EXIT_MSG_PAT, content)) {
            return new Message(MessageType.EXIT, user.getUsername(),
                    null, null);
        }
        if (Pattern.matches(TXT_MSG_PAT, content)) {
            String[] parts = content.split(" ", 2);
            return new Message(MessageType.TEXT, user.getUsername(),
                    parts[0].substring(1), parts[1]);
        }

        throw new MessageException("Unknown message type");
    }

}
